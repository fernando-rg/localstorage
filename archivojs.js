

window.addEventListener('DOMContentLoaded', (event) => {
    let boton = document.getElementById('submit')
    let botonEliminar = document.getElementById('eliminar')
    let responsesLocal = allStorageResponse();
    let select = document.getElementById('responses');

    addOptionsResponse(responsesLocal)

    select.addEventListener("change", function (){
        let responseNow = JSON.parse(responsesLocal[select.value])
        document.getElementById('fname').value = responseNow.nombre
        document.getElementById('lname').value = responseNow.apellido
    })

    boton.addEventListener("click", function (){

        let incremental = 0
        if(localStorage.getItem('incremental')){
            incremental = localStorage.getItem('incremental')
        }
        incremental++
        localStorage.setItem('incremental', incremental)

        let nameSave = "response-" + prompt("Enter your name : ", incremental );

        console.log(nameSave)

        let nombre = document.getElementById('fname')
        let apellido = document.getElementById('lname')
        let nombreValue = nombre.value
        let apellidoValue = apellido.value

        let response = {nombre: nombreValue, apellido:apellidoValue}

        debugger
        localStorage.setItem(nameSave, JSON.stringify(response))

    });

    botonEliminar.addEventListener("click", function (){
        console.log("Elimino:" + select.value)
        localStorage.removeItem(select.value)
    });
});


function allStorageResponse() {

    var values = [], keys = Object.keys(localStorage);

    keys = keys.filter(key=> key.indexOf('response') != -1)
    let i = keys.length;

    while ( i-- ) {
        values[keys[i]] = localStorage.getItem(keys[i]);
    }

    return values;
}

function addOptionsResponse (responses){
    var select = document.getElementById('responses');

    var keys = Object.keys(responses)
    keys.forEach(function (key, value){
        console.log(key)
        var opt = document.createElement('option');
        opt.value = key;
        opt.innerHTML = key;
        select.appendChild(opt);
    })
}